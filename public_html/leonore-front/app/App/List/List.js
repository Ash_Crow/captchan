'use strict';

angular.module('transcript.app.list', ['ui.router'])

    .config(['$stateProvider', function($stateProvider) {
        $stateProvider.state('transcript.app.list', {
            views: {
                "page" : {
                    templateUrl: '/hackathon-an/leonore-front/app/App/List/List.html',
                    controller: 'AppListCtrl'
                }
            },
            ncyBreadcrumb: {
                parent: 'transcript.app.home',
                label: 'Liste des légionnaires'
            },
            tfMetaTags: {
                title: "Liste des légionnaires",
            },
            url: '/liste',
            resolve: {
                entities: function(DataCsvService) {
                    return DataCsvService.parseCSV().then(function(data) {
                        return DataCsvService.convertCSV(data.data);
                    });
                }
            }
        })
    }])

    .controller('AppListCtrl', ['$log', '$rootScope','$scope', '$http', '$sce', '$state', 'entities', function($log, $rootScope, $scope, $http, $sce, $state, entities) {
        $scope.entities = entities;
        console.log(entities);


        /* ---------------------------------------------------------------------------------------------------------- */
        /* Pagination system */
        /* ---------------------------------------------------------------------------------------------------------- */
        $scope.itemsPerPage = 1000;
        $scope.$watch('entities', function() {
            $scope.totalItems = $scope.entities.length;
            $scope.currentPage = 1;
        });

        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.pageChanged = function() {
            $log.debug('Page changed to: ' + $scope.currentPage);
        };
        /* ---------------------------------------------------------------------------------------------------------- */
    }])
;