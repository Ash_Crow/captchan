'use strict';

angular.module('transcript.app.entity', ['ui.router'])

    .config(['$stateProvider', function($stateProvider) {
        $stateProvider.state('transcript.app.entity', {
            views: {
                "page" : {
                    templateUrl: '/hackathon-an/leonore-front/app/App/Entity/Entity.html',
                    controller: 'AppEntityCtrl'
                },
                /*"comment@transcript.app.entity" : {
                    templateUrl: '/hackathon-an/leonore-front/app/System/Comment/tpl/Thread.html',
                    controller: 'SystemCommentCtrl'
                }*/
            },
            ncyBreadcrumb: {
                parent: 'transcript.app.list',
                label: '{{ entity.prenom }} {{ entity.nom }}'
            },
            tfMetaTags: {
                title: "{{ entity.prenom }} {{ entity.nom }}",
            },
            url: '/entity/{id1}/{id2}/{id3}',
            resolve: {
                entity: function(EntityService, $transition$) {
                    let id = $transition$.params().id1+"/"+$transition$.params().id2+"/"+$transition$.params().id3;
                    return EntityService.getEntityFromCSV(id);
                }/*,
                thread: function(CommentService, $transition$) {
                    return CommentService.getThread('entity-'+$transition$.params().id);
                }*/
            }
        })
    }])

    .controller('AppEntityCtrl', ['$log', '$rootScope','$scope', '$http', '$sce', '$state', 'entity', function($log, $rootScope, $scope, $http, $sce, $state, entity) {
        $scope.entity = entity;
        console.log(entity);

        let images = $scope.entity.video.split(" ; ");
        $scope.entity.images = [];
        for (let id in images) {
            $scope.entity.images.push(images[id]);
        }
    }])
;