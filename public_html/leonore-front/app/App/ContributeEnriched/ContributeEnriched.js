'use strict';

angular.module('transcript.app.contribute-enriched', ['ui.router'])

    .config(['$stateProvider', function($stateProvider) {
        $stateProvider.state('transcript.app.contribute-enriched', {
            views: {
                "page" : {
                    templateUrl: '/hackathon-an/leonore-front/app/App/ContributeEnriched/ContributeEnriched.html',
                    controller: 'AppContributeCtrl'
                }
            },
            ncyBreadcrumb: {
                parent: 'transcript.app.entity',
                label: 'Contribution'
            },
            tfMetaTags: {
                title: "Contribution",
            },
            url: '/contribute/enrichie/{id1}/{id2}/{id3}/{image}',
            resolve: {
                entity: function(EntityService, $transition$) {
                    let id = $transition$.params().id1+"/"+$transition$.params().id2+"/"+$transition$.params().id3;
                    return EntityService.getEntityFromCSV(id);
                },
                image: function(ImageService, $transition$) {
                    return null;
                }
            }
        })
    }])

    .controller('AppContributeCtrl', ['$log', '$rootScope','$scope', '$http', '$sce', '$state', '$transition$', '$timeout', 'entity', 'image', function($log, $rootScope, $scope, $http, $sce, $state, $transition$, $timeout, entity, image) {
        $scope.entity = entity;
        $scope.image = $transition$.params().image;
        $scope.imageURL = "/hackathon-an/leonore-front/app/web/images/data/"+$scope.image;
        console.log(entity);

        let images = $scope.entity.video.split(" ; ");
        $scope.entity.images = [];
        for (let id in images) {
            $scope.entity.images.push(images[id]);
        }


        $scope.setInfo = false;

        /* ----------------------------------------------------------------------------- */
        /* Contribution */
        /* ----------------------------------------------------------------------------- */
        $scope.init = function() {
            $scope.actions = {
                crop: false,
                detourer: false,
                reporter: false,
                formValue: '',
                formValid: false,
                add: false,
                rangLegion: false,
                addRangLegion: false,
                dateLegion: false,
                addDateLegion: false,
                check: false,
                prenom: false,
                nom: false,
                addNom: false,
                nom2: false,
                surnom: false,
                nomJeuneFille: false,
                nomMari: false,
                prenomMari: false,
                profession: false,
                addProfession: false,
                domicile: false,
                sexe: false,
                situationFamiliale: false,
                birthdate: false,
                birthplace: false,
                deathdate: false,
                deathplace: false
            };
        };
        $scope.init();

        $scope.coordinates = null;

        $scope.selected = function(x) {
            $scope.coordinates = x;
            console.log("selected",x);
        };

        $scope.contributeTo = function(field) {
            $scope.init();
            $scope.actions.crop = true;

            if(field === "rangLegion") {
                $scope.actions.rangLegion = true;
            } else if(field === "dateLegion") {
                $scope.actions.dateLegion = true;
            } else if(field === "prenom") {
                $scope.actions.prenom = true;
            } else if(field === "nom") {
                $scope.actions.nom = true;
            } else if(field === "nom2") {
                $scope.actions.nom2 = true;
            } else if(field === "surnom") {
                $scope.actions.surnom = true;
            } else if(field === "nomJeuneFille") {
                $scope.actions.nomJeuneFille = true;
            } else if(field === "nomMari") {
                $scope.actions.nomMari = true;
            } else if(field === "prenomMari") {
                $scope.actions.prenomMari = true;
            } else if(field === "profession") {
                $scope.actions.profession = true;
            } else if(field === "domicile") {
                $scope.actions.domicile = true;
            } else if(field === "sexe") {
                $scope.actions.sexe = true;
            } else if(field === "situationFamiliale") {
                $scope.actions.situationFamiliale = true;
            } else if(field === "birthdate") {
                $scope.actions.birthdate = true;
            } else if(field === "birthplace") {
                $scope.actions.birthplace = true;
            } else if(field === "deathdate") {
                $scope.actions.deathdate = true;
            } else if(field === "deathplace") {
                $scope.actions.deathplace = true;
            }

            $scope.$watchGroup(['coordinates', 'actions.formValue'], function(){
                if($scope.coordinates !== null) {
                    $scope.actions.detourer = true;
                }
                if($scope.actions.formValue !== '') {
                    $scope.actions.reporter = true;
                }
                if($scope.coordinates !== null && $scope.actions.formValue !== '') {
                    $('#validationForm').removeClass('disabled');
                }
            });
        };

        $scope.addTo = function(field) {
            $scope.init();
            $scope.actions.crop = true;
            $scope.actions.add = true;

            if(field === "rangLegion") {
                $scope.actions.addRangLegion = true;
            } else if(field === "dateLegion") {
                $scope.actions.addDateLegion = true;
            } else if(field === "nom") {
                $scope.actions.addNom = true;
            } else if(field === "profession") {
                $scope.actions.addProfession = true;
            }

            $scope.$watchGroup(['coordinates', 'actions.formValue'], function(){
                if($scope.coordinates !== null) {
                    $scope.actions.detourer = true;
                }
                if($scope.actions.formValue !== '') {
                    $scope.actions.reporter = true;
                }
                if($scope.coordinates !== null && $scope.actions.formValue !== '') {
                    $('#validationForm').removeClass('disabled');
                }
            });
        };

        $scope.valid = {
            form: {
                property: null,
                coordinates: [],
                contribution: null,
                idLegionnaire: null,
                idDocument: null
            }
        };
        $scope.valid.action = function() {
            if($scope.actions.rangLegion === true) {
                $scope.valid.form.property = "rangLegion" ;
            } if($scope.actions.dateLegion === true) {
                $scope.valid.form.property = "dateLegion" ;
            } else if($scope.actions.prenom === true ) {
                $scope.valid.form.property = "prenom";
            } else if($scope.actions.nom === true ) {
                $scope.valid.form.property = "nom";
            } else if($scope.actions.nom2 === true ) {
                $scope.valid.form.property = "nom2";
            } else if($scope.actions.surnom === true ) {
                $scope.valid.form.property = "surnom";
            } else if($scope.actions.nomJeuneFille === true ) {
                $scope.valid.form.property = "nomJeuneFille";
            } else if($scope.actions.nomMari === true ) {
                $scope.valid.form.property = "nomMari";
            } else if($scope.actions.prenomMari === true ) {
                $scope.valid.form.property = "prenomMari";
            } else if($scope.actions.profession === true ) {
                $scope.valid.form.property = "profession";
            } else if($scope.actions.domicile === true ) {
                $scope.valid.form.property = "domicile";
            } else if($scope.actions.sexe === true ) {
                $scope.valid.form.property = "sexe";
            } else if($scope.actions.situationFamiliale === true ) {
                $scope.valid.form.property  ="situationFamiliale";
            } else if($scope.actions.birthdate === true ) {
                $scope.valid.form.property = "birthdate";
            } else if($scope.actions.birthplace === true ) {
                $scope.valid.form.property = "birthplace";
            } else if($scope.actions.deathdate === true ) {
                $scope.valid.form.property = "deathdate";
            } else if($scope.actions.deathplace === true ) {
                $scope.valid.form.property = "deathplace";
            }

            $scope.valid.form.coordinates = $scope.coordinates;
            $scope.valid.form.contribution = $scope.actions.formValue;
            $scope.valid.form.idLegionnaire = $scope.entity.id;
            $scope.valid.form.idDocument = $transition$.params().image;
            console.log($scope.valid.form);

            $scope.actions.check = true;
            $timeout(function() {$scope.init();}, 5000);
        };

    }])
;