'use strict';

angular.module('transcript.app.home', ['ui.router'])

    .config(['$stateProvider', function($stateProvider) {
        $stateProvider.state('transcript.app.home', {
            views: {
                "page" : {
                    templateUrl: '/hackathon-an/leonore-front/app/App/Home/Home.html',
                    controller: 'AppHomeCtrl'
                }
            },
            ncyBreadcrumb: {
                label: 'Accueil'
            },
            tfMetaTags: {
                title: 'Accueil',
            },
            url: '/',
            resolve: {
                entities: function(DataCsvService) {
                    return DataCsvService.parseCSV().then(function(data) {
                        return DataCsvService.convertCSV(data.data);
                    });
                }
            }
        })
    }])

    .controller('AppHomeCtrl', ['$log', '$rootScope','$scope', '$http', '$sce', '$state', '$filter', 'entities', 'SearchService', function($log, $rootScope, $scope, $http, $sce, $state, $filter, entities, SearchService) {
        $scope.entities = entities; console.log($scope.entities);
        $scope.limitToResults = 50;

        /* -- Search interface ------------------------------------------------ */
        /* -- Definition of the fields --------------------------------------------------------------- */
        $scope.results = [];
        $scope.search = {
            form: {
                nom: null,
                placeOfBirth: null,
                dateOfBirth: null,
                sexe: null
            },
            values: {
                nom: SearchService.dataset($scope.entities, "nom", "string"),
                placeOfBirth: SearchService.dataset($scope.entities, "birthplace", "string"),
                dateOfBirth: SearchService.dataset($scope.entities, "birthdate", "string"),
                sexe: SearchService.dataset($scope.entities, "sexe", "string")
            },
            checks: {
                nom: false,
                placeOfBirth: false,
                dateOfBirth: false,
                sexe: false
            }
        };

        console.log($scope.search.values.nom);
        console.log($scope.search.values.placeOfBirth);
        console.log($scope.search.values.dateOfBirth);
        console.log($scope.search.values.sexe);
        /* -- End : Definition of the fields --------------------------------------------------------- */

        /* -- Fields watching ------------------------------------------------------------------------ */
        $scope.$watch('search.form.nom', function() {
            if($scope.search.form.nom !== undefined) {
                if ($scope.search.form.nom !== null && $scope.search.form.nom !== "" && $scope.search.form.nom.originalObject !== undefined) {
                    $scope.search.form.nom = $scope.search.form.nom.originalObject.value;
                    console.log($scope.search.form.nom);
                }
                refresh();
            }
        });

        $scope.$watch('search.form.birthplace', function() {
            if($scope.search.form.birthplace !== undefined) {
                if ($scope.search.form.birthplace !== null && $scope.search.form.birthplace !== "" && $scope.search.form.birthplace.originalObject !== undefined) {
                    $scope.search.form.birthplace = $scope.search.form.birthplace.originalObject.value;
                    console.log($scope.search.form.birthplace);
                }
                refresh();
            }
        });

        $scope.$watch('search.form.birthdate', function() {
            if($scope.search.form.birthdate !== undefined) {
                if ($scope.search.form.birthdate !== null && $scope.search.form.birthdate !== "" && $scope.search.form.birthdate.originalObject !== undefined) {
                    $scope.search.form.birthdate = $scope.search.form.birthdate.originalObject.value;
                    console.log($scope.search.form.birthdate);
                }
                refresh();
            }
        });

        $scope.$watch('search.form.sexe', function() {
            if($scope.search.form.sexe !== undefined) {
                if ($scope.search.form.sexe !== null && $scope.search.form.sexe !== "" && $scope.search.form.sexe.originalObject !== undefined) {
                    $scope.search.form.sexe = $scope.search.form.sexe.originalObject.value;
                    console.log($scope.search.form.sexe);
                }
                refresh();
            }
        });

        $scope.$watchGroup(['search.checks.nom', 'search.checks.birthdate', 'search.checks.birthplace', 'search.checks.sexe'], function(){
            refreshEmpty();
        });
        /* -- End : Fields watching ------------------------------------------------------------------ */

        function refreshEmpty() {
            let toEntities = $scope.entities;
            if($scope.search.checks.nom !== false) {
                $scope.results = $filter('filter')(toEntities, {'nom': ''}, true);
            } else if($scope.search.checks.birthdate !== false) {
                $scope.results = $filter('filter')(toEntities, {'birthdate': ''}, true);
            } else if($scope.search.checks.birthplace !== false) {
                $scope.results = $filter('filter')(toEntities, {'birthplace': ''}, true);
            } else if($scope.search.checks.sexe !== false) {
                $scope.results = $filter('filter')(toEntities, {'sexe': ''}, true);
            }

            $scope.results = $filter('shuffle')($scope.results);
        }

        function refresh() {
            let toEntities = $scope.entities;
            let toForm = $scope.search.form;
            console.log(toForm);
            $scope.results = SearchService.search(toEntities, toForm);

            if($scope.search.form.sexe !== undefined || $scope.search.form.birthdate !== undefined || $scope.search.form.birthplace !== undefined || $scope.search.form.nom !== undefined) {
                $scope.limitToResults = 1000;
            } else {
                $scope.limitToResults = 50;
            }

            $scope.results = $filter('shuffle')($scope.results);
            console.log($scope.results);

            //setMarkers();
        }

        /* -- Setting up map ----------------------------------------------------------------- */
        // Doc is here: http://tombatossals.github.io/angular-leaflet-directive/#!/examples/center
        /*angular.extend($scope, {
            center: {
                lat: 49.9128,
                lng: 3.7587,
                zoom: 6
            },
            tiles: {
                url: "http://a.tile.openstreetmap.fr/{z}/{x}/{y}.png"
            },
            markers: {

            },
            defaults: {
                scrollWheelZoom: false
            },
            legend: {
                position: 'bottomleft',
                colors: [ '#000000' ],
                labels: [ 'Lieux de décès' ]
            }
        });

        function setMarkers() {
            let markers = {};
            for(let result in $scope.results) {
                let entity = $scope.results[result];

                if(entity.will.testator.placeOfDeathNormalized !== undefined && entity.will.testator.placeOfDeathNormalized.geographicalCoordinates !== undefined) {
                    let coord = entity.will.testator.placeOfDeathNormalized.geographicalCoordinates.split('+');
                    let id = "maker"+entity.will.testator.id;
                    let iconMarker = "marker-icon";

                    if(entity._embedded.status === "todo") {iconMarker = "marker-icon-danger";}
                    else if(entity._embedded.status === "transcription") {iconMarker = "marker-icon-warning";}
                    else if(entity._embedded.status === "validation") {iconMarker = "marker-icon-primary";}
                    else if(entity._embedded.status === "validated") {iconMarker = "marker-icon-success";}

                    let marker = {
                        lat: parseFloat(coord[0]),
                        lng: parseFloat(coord[1]),
                        message: '<a href="#!/entity/'+entity.id+'">'+entity.name+' décédé à '+entity.birthplace+'</a>',
                        focus: false,
                        draggable: false,
                        icon: {
                            iconUrl: '/webapp/app/web/images/markers/'+iconMarker+'.png',
                            iconAnchor: [8, 23],
                            popupAnchor: [0, -22],
                        }
                    };
                    markers[id] = marker;
                }
            }
            $scope.markers = markers;
        }*/
        /* -- End : Setting up map ----------------------------------------------------------- */
        /* -- Search interface ------------------------------------------------ */
    }])
;