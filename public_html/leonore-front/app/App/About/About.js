'use strict';

angular.module('transcript.app.about', ['ui.router'])

    .config(['$stateProvider', function($stateProvider) {
        $stateProvider.state('transcript.app.about', {
            views: {
                "page" : {
                    templateUrl: '/hackathon-an/leonore-front/app/App/About/About.html',
                    controller: 'AppAboutCtrl'
                }
            },
            ncyBreadcrumb: {
                parent: 'transcript.app.home',
                label: 'À propos'
            },
            tfMetaTags: {
                title: 'À propos',
            },
            url: '/about',
            resolve: {
                /*entities: function(EntityService) {
                    return EntityService.getEntities("search");
                }*/
            }
        })
    }])

    .controller('AppHomeCtrl', ['$log', '$rootScope','$scope', '$http', '$sce', '$state', '$filter', function($log, $rootScope, $scope, $http, $sce, $state, $filter) {

    }])
;