'use strict';

angular.module('transcript', ['ui.router'])

    .config(['$stateProvider', function($stateProvider) {
        $stateProvider.state('transcript', {
            abstract: true,
            views: {
                "navbar" : {
                    templateUrl: '/hackathon-an/leonore-front/app/System/Navbar/Navbar.html',
                    //controller: 'TranscriptCtrl'
                },
                "page" : {
                    templateUrl: '/hackathon-an/leonore-front/app/Transcript/Transcript.html',
                    controller: 'TranscriptCtrl'
                },
                "footer" : {
                    templateUrl: '/hackathon-an/leonore-front/app/System/Footer/Footer.html',
                    //controller: 'TranscriptCtrl'
                }
            },
            url: '',
            resolve: {
                appPreference: function(AppService) {
                    return AppService.getPreference();
                },
                user: function(UserService) {
                    return UserService.getCurrent();
                }
            }
        })
    }])

    .controller('TranscriptCtrl', ['$log', '$rootScope','$scope', '$http', '$sce', '$state', 'user', 'appPreference', 'tfMetaTags', function($log, $rootScope, $scope, $http, $sce, $state, user, appPreference, tfMetaTags) {
        if (user !== null) {
            if($rootScope.user === undefined) {
                $rootScope.user = user;
            }
            $log.debug($rootScope.user);
        }
        $rootScope.preferences = appPreference;
        tfMetaTags.setTitleSuffix(' | '+$rootScope.preferences.projectTitle+tfMetaTags.getTitleSuffix());
    }])
;