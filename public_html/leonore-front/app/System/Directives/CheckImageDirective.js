'use strict';

angular.module('transcript.directive.check-image', ['ui.router'])

    .directive('checkImage', ['$http', function ($http) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                attrs.$observe('ngSrc', function(ngSrc) {
                    $http.get(ngSrc).then(
                        function(){
                        },
                        function(){
                            element.attr('src', '/hackathon-an/leonore-front/app/web/images/no-images.png'); // set default image
                        }
                    );
                });
            }
        };
    }])

;