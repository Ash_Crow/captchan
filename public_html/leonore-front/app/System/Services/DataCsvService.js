'use strict';

angular.module('transcript.service.data-csv', ['ui.router'])

    .service('DataCsvService', function($log, $http, $rootScope) {
        let functions = {
            /*CSVtoArray: function(text) {
                let re_valid = /^\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*(?:,\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*)*$/;
                let re_value = /(?!\s*$)\s*(?:'([^'\\]*(?:\\[\S\s][^'\\]*)*)'|"([^"\\]*(?:\\[\S\s][^"\\]*)*)"|([^,'"\s\\]*(?:\s+[^,'"\s\\]+)*))\s*(?:,|$)/g;
                // Return NULL if input string is not well formed CSV string.
                if (!re_valid.test(text)) {return null;}
                let a = [];                     // Initialize array to receive values.
                text.replace(re_value, // "Walk" the string using replace with callback.
                    function(m0, m1, m2, m3) {
                        // Remove backslash from \' in single quoted values.
                        if (m1 !== undefined) {a.push(m1.replace(/\\'/g, "'"));}
                        // Remove backslash from \" in double quoted values.
                        else if (m2 !== undefined) {a.push(m2.replace(/\\"/g, '"'));}
                        else if (m3 !== undefined) {a.push(m3);}
                        return ''; // Return empty string.
                    });
                // Handle special case of empty last value.
                if (/,\s*$/.test(text)) {a.push('');}
                return a;
            },*/
            parseCSV: function() {
                return $http.get('/hackathon-an/leonore-front/app/web/data/a.csv').then(function(response) {
                    return Papa.parse(response.data, {
                        complete: function(results) {
                            let entities = [];
                            console.log("Finished:", results.data);
                        },
                    });
                });
            },
            convertCSV: function(items) {
                console.log(items);
                let entities = [];
                for (let id in items) {
                    let data = items[id];
                    entities.push({
                        id1: data[0].split('/')[0],
                        id2: data[0].split('/')[1],
                        id3: data[1],
                        id: data[0]+'/'+data[1],
                        cote: data[0],
                        dossier: data[1],
                        nom: data[2],
                        prenom: data[3],
                        birthdate: data[4],
                        dateRev: data[5],
                        birthplace: data[6],
                        code: data[7],
                        nom2: data[8],
                        nomJeuneFille: data[9],
                        nomMari: data[10],
                        prenomMari: data[11],
                        sexe: data[12],
                        situationFamiliale:data[13],
                        surnom: data[14],
                        notes: data[15],
                        refMistral: data[16],
                        video: data[17]
                    });
                }

                console.log(entities);
                return entities;
            }
        };
        return functions;
    })
;