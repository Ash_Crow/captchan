'use strict';

angular.module('transcriptApp', [
        'ui.router',
        'ngRoute',
        'ngAnimate',
        'ngCookies',
        'angular-oauth2',
        'http-auth-interceptor',
        'angular-flash.service',
        'angular-flash.flash-alert-directive',
        'ui.bootstrap',
        'ncy-angular-breadcrumb',
        'ngFileUpload',
        'angucomplete',
        'leaflet-directive',
        'angular.filter',
        'prettyXml',
        'permission',
        'permission.ui',
        'angular-loading-bar',
        'ui.openseadragon',
        'tf.metatags',
        'FBAngular',
        'transcript',
        'transcript.app',
        'transcript.app.about',
        'transcript.app.contribute',
        'transcript.app.contribute-enriched',
        'transcript.app.entity',
        'transcript.app.home',
        'transcript.app.list',
        'transcript.app.search',
        'transcript.app.security',
        'transcript.app.security.check',
        'transcript.app.security.confirm',
        'transcript.app.security.login',
        'transcript.app.security.logout',
        'transcript.app.security.register',
        'transcript.app.security.resetting',
        'transcript.app.security.resetting.check',
        'transcript.app.security.resetting.request',
        'transcript.app.security.resetting.reset',
        'transcript.app.user',
        'transcript.app.user.avatar',
        'transcript.app.user.change-password',
        'transcript.app.user.edit',
        'transcript.app.user.preferences',
        'transcript.app.user.private-message',
        'transcript.app.user.private-message.list',
        'transcript.app.user.private-message.thread',
        'transcript.app.user.profile',
        'transcript.system.comment',
        'transcript.system.error',
        'transcript.system.error.403',
        'transcript.system.error.404',
        'transcript.directive.compile',
        'transcript.directive.check-image',
        'transcript.directive.crop',
        'transcript.directive.mwConfirmClick',
        'transcript.directive.tooltip',
        'transcript.filter.arrayRender',
        'transcript.filter.booleanRender',
        'transcript.filter.classicDate',
        'transcript.filter.contentTypeName',
        'transcript.filter.encodeXML',
        'transcript.filter.imageRender',
        'transcript.filter.internalAttributesRender',
        'transcript.filter.internalLinksRender',
        'transcript.filter.prefixRender',
        'transcript.filter.objectToId',
        'transcript.filter.resourceTypeName',
        'transcript.filter.shuffle',
        'transcript.filter.taxonomyEntityNameConstruct',
        'transcript.filter.taxonomyName',
        'transcript.filter.trainingTypePageName',
        'transcript.filter.transcriptionStatusName',
        'transcript.filter.trustAsHtml',
        'transcript.filter.reverse',
        'transcript.filter.roleName',
        'transcript.filter.sourceRender',
        'transcript.filter.status',
        'transcript.filter.stringToArray',
        'transcript.filter.stringToDate',
        'transcript.filter.ucFirstStrict',
        'transcript.filter.willNumberFormat',
        'transcript.filter.userIDFromName',
        'transcript.service.access',
        'transcript.service.app',
        'transcript.service.comment',
        'transcript.service.comment-log',
        'transcript.service.contributor',
        'transcript.service.data-csv',
        'transcript.service.entity',
        'transcript.service.geonames',
        'transcript.service.image',
        'transcript.service.search',
        'transcript.service.user',
        'transcript.service.user-preference'
    ]).
    config(['$stateProvider','$httpProvider', '$urlRouterProvider', '$qProvider', '$injector', 'flashProvider', 'tfMetaTagsProvider', 'OAuthTokenProvider', '$logProvider', '$locationProvider', function($stateProvider, $httpProvider, $urlRouterProvider, $qProvider, $injector, flashProvider, tfMetaTagsProvider, OAuthTokenProvider, $logProvider, $locationProvider) {
        //$locationProvider.html5Mode(true);
        $urlRouterProvider.otherwise('/');
        //$urlRouterProvider.deferIntercept();
        $qProvider.errorOnUnhandledRejections(false);

        OAuthTokenProvider.configure({
            name: "transcript_security_token_access",
            options: {
                secure: false,
            }
        });

        /* ------------------------------------------------------ */
        /* Flash management */
        /* ------------------------------------------------------ */
        flashProvider.errorClassnames.push('alert-danger');
        flashProvider.warnClassnames.push('alert-warning');
        flashProvider.infoClassnames.push('alert-info');
        flashProvider.successClassnames.push('alert-success');
        /* ------------------------------------------------------ */

        /* ------------------------------------------------------ */
        /* Meta tag management */
        /* ------------------------------------------------------ */
        tfMetaTagsProvider.setDefaults({
            title: 'Testaments de Poilus'
        });
        tfMetaTagsProvider.setTitleSuffix('');
        tfMetaTagsProvider.setTitlePrefix('');
        /* ------------------------------------------------------ */


        $logProvider.debugEnabled(false);
    }])
    .run(['$log', '$rootScope', '$http', '$injector', '$location', 'authService', '$state', '$cookies', '$filter', '$window', 'PermRoleStore', 'PermPermissionStore', 'UserService', 'OAuth', 'OAuthToken', 'DataCsvService', function($log, $rootScope, $http, $injector, $location, authService, $state, $cookies, $filter, $window, PermRoleStore, PermPermissionStore, UserService, OAuth, OAuthToken, DataCsvService) {
        /* -- Parameters management ------------------------------------------------------ */
        let parameters = YAML.load('/hackathon-an/leonore-front/app/parameters.yml');
        $rootScope.version = parameters.version;
        $rootScope.api = parameters.api;
        $rootScope.api_web = parameters.api_web;
        $rootScope.iiif = parameters.iiif;
        $rootScope.env = parameters.env;
        $rootScope.webapp = {
            strict: parameters.webapp.strict,
            resources: parameters.webapp.resources
        };
        $rootScope.siteURL = parameters.siteURL;
        $rootScope.client_id = parameters.client_id;
        $rootScope.client_secret = parameters.client_secret;
        /* -- End : Parameters management ------------------------------------------------ */

        /* -- OAuth management ----------------------------------------------------------- */
        OAuth.configure({
            baseUrl: $rootScope.api,
            clientId: $rootScope.client_id,
            clientSecret: $rootScope.client_secret,
            grantPath: '/oauth/v2/token',
        });


        $rootScope.$on('oauth:error', function(event, rejection) {
            // Ignore `invalid_grant` error - should be catched on `LoginController`.
            if ('invalid_grant' === rejection.data.error) {
                return;
            }

            // Refresh token when a `invalid_token` error occurs.
            if ('invalid_token' === rejection.data.error) {
                return OAuth.getRefreshToken();
            }

            // Redirect to `/login` with the `error_reason`.
            return $state.go('transcrip.app.security.login'); // Need to handle error : rejection.data.error
        });
        /* -- End : OAuth management ----------------------------------------------------- */

        /* -- Permission management ------------------------------------------------------ */
        PermPermissionStore
            .definePermission('adminAccess', function () {
                if($rootScope.user !== undefined) {
                    return ($rootScope.user !== undefined && $filter('contains')($rootScope.user.roles, 'ROLE_ADMIN'));
                } else {
                    return UserService.getCurrent().then(function() {
                        $log.debug(!!($rootScope.user !== undefined && $filter('contains')($rootScope.user.roles, 'ROLE_ADMIN')));
                        return ($rootScope.user !== undefined && $rootScope.user.roles !== undefined && $filter('contains')($rootScope.user.roles, 'ROLE_ADMIN'));
                    });
                }
            });
        PermPermissionStore
            .definePermission('transcriptAccess', function () {
                if($rootScope.user !== undefined) {
                    return ($rootScope.user !== undefined);
                } else {
                    return UserService.getCurrent().then(function() {
                        $log.debug($rootScope.user);
                        return ($rootScope.user !== undefined);
                    });
                }
            });

        PermRoleStore
            .defineManyRoles({
                'ROLE_ADMIN': ['adminAccess', 'transcriptAccess'],
                'ROLE_USER': ['transcriptAccess']
            });
        /* -- End : Permission management ------------------------------------------------ */

        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('.tooltip').tooltip();
        });

    }])
;